#include <KIO/WorkerBase>

#include <QCoreApplication>

class FizzBuzz : public KIO::WorkerBase
{
public:
    FizzBuzz(const QByteArray &protocol, const QByteArray &pool, const QByteArray &app);

    KIO::WorkerResult get(const QUrl &url) override;
};

class KIOPluginForMetaData : public QObject
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.kde.kio.worker.fizzbuzz" FILE "fizzbuzz.json")
};

extern "C" {
int Q_DECL_EXPORT kdemain(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    app.setApplicationName(QStringLiteral("kio_fizzbuzz"));

    FizzBuzz worker(argv[1], argv[2], argv[3]);
    worker.dispatchLoop();
    return 0;
}
}

FizzBuzz::FizzBuzz(const QByteArray &protocol, const QByteArray &pool, const QByteArray &app)
    : WorkerBase(protocol, pool, app)
{
}

KIO::WorkerResult FizzBuzz::get(const QUrl &url)
{
    bool ok;
    int number = url.path().mid(1).toInt(&ok);

    if (!ok) {
        return KIO::WorkerResult::fail(KIO::ERR_MALFORMED_URL, QStringLiteral("Not a number. Expected URL like 'fizzbuzz:/4'"));
    }

    mimeType(QStringLiteral("text/plain"));

    if (number % 3 == 0 && number % 5 != 0) {
        data("Fizz\n");
    } else if (number % 3 != 0 && number % 5 == 0) {
        data("Buzz\n");
    } else if (number % 3 == 0 && number % 5 == 0) {
        data("FizzBuzz\n");
    } else {
        data(QString::number(number).toLatin1() + "\n");
    }

    return KIO::WorkerResult::pass();
}

#include "kio_fizzbuzz.moc"
