# kio-fizzbuzz

A KIO worker implementing FizzBuzz

# Usage

`kioclient cat fizzbuzz:/1` gives 1

`kioclient cat fizzbuzz:/3` gives Fizz

`kioclient cat fizzbuzz:/5` gives Buzz

`kioclient cat fizzbuzz:/15` gives FizzBuzz

Or use from any KIO-powered application that displays text, e.g. Kate

# Why?

Why not?
